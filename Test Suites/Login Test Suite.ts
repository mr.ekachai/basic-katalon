<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0df26829-7389-4a7b-8dd5-89a2d6668c46</testSuiteGuid>
   <testCaseLink>
      <guid>62784528-d517-42dc-8604-e8e5a0ef201a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo Cura/Open Website</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51adb6ee-3c1d-46b9-83f1-5bd867b9c951</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo Cura/Login Failed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>310c3ec8-9cfb-48c5-bb0d-1c480563eb42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo Cura/Login Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
